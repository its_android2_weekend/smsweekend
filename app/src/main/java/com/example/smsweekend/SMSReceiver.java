package com.example.smsweekend;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class SMSReceiver extends BroadcastReceiver {
    private static final String TAG = SMSReceiver.class.getSimpleName();
    public static final String MY_SMS_ACTION_SEND = "MY_SMS_ACTION_SEND";

    @Override
    public void onReceive(Context context, Intent intent) {
        //---Get the SMS message;
        Bundle bundle = intent.getExtras();
        SmsMessage[] smsMessages = null;
        String str = "SMS From ";
        if(bundle!=null){
            smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            for (SmsMessage smsMessage : smsMessages ){
                str += smsMessage.getMessageBody();
            }
        }
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
        Log.e(TAG,str);
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SMSReceiver.MY_SMS_ACTION_SEND);
        broadcastIntent.putExtra("sms",str);
        context.sendBroadcast(broadcastIntent);
    }
}
