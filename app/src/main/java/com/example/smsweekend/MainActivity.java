package com.example.smsweekend;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int SEND_SMS_REQUEST_CODE = 168;
    private static final int READ_PHONE_STATE_REQUEST_CODE = 169;
    private EditText etPhoneNumber;
    private EditText etSmsBody;
    private Button btnSendSMS;
    private Button btnSendSMSIntent;
    private TextView textView;
    private Button btnSendMail;

    String phoneNumber;
    String smsBody;

    IntentFilter intentFilter;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            textView.setText(intent.getExtras().getString("sms"));
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver,intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSendSMS = findViewById(R.id.btnSendSMS);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        etSmsBody = findViewById(R.id.etSmsBody);
        btnSendSMSIntent = findViewById(R.id.btnSendSMSIntent);
        textView = findViewById(R.id.textView);
        btnSendMail = findViewById(R.id.btnSendMail);


        //Bind Control Listener;
        btnSendSMS.setOnClickListener(this);
        btnSendSMSIntent.setOnClickListener(this);
        btnSendMail.setOnClickListener(this);

        //Handle Request runtime Permission;
        int checked = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);

        if(checked != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS},
                    SEND_SMS_REQUEST_CODE
            );
        }
        //Initilize Itent Fileter
        intentFilter = new IntentFilter();
        intentFilter.addAction(SMSReceiver.MY_SMS_ACTION_SEND);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case SEND_SMS_REQUEST_CODE:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Send SMS Permission Granted", Toast.LENGTH_SHORT).show();
                }
                break;
            case READ_PHONE_STATE_REQUEST_CODE:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Send SMS Permission Granted", Toast.LENGTH_SHORT).show();

                    phoneNumber = etPhoneNumber.getText().toString();
                    smsBody = etSmsBody.getText().toString();

                    sendSMS(phoneNumber,smsBody);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        phoneNumber = etPhoneNumber.getText().toString();
        smsBody = etSmsBody.getText().toString();

        switch (view.getId()){
            case R.id.btnSendSMS:

                sendSMS(phoneNumber,smsBody);

                break;
            case R.id.btnSendSMSIntent:
                sendMessageViaIntent(phoneNumber,smsBody);
                break;
            case R.id.btnSendMail:
                String[] to = {"lengsovandara@gmail.com"};
                String[] cc = {"lengsovandara@yahoo.com"};
                String subject = "Hello From Android Weekend Class";
                String messageBody = "Message Body";

                SendEmail(to,cc,subject,messageBody);
                break;
        }
    }

    private void SendEmail(String[] to, String[] cc, String subject, String messageBody) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL,to);
        intent.putExtra(Intent.EXTRA_CC,cc);
        intent.putExtra(Intent.EXTRA_SUBJECT,subject);
        intent.putExtra(Intent.EXTRA_TEXT,messageBody);
        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent,"Email"));
    }

    private void sendMessageViaIntent(String phoneNumber, String smsBody) {
        Intent intent = new Intent(
                Intent.ACTION_VIEW,
                Uri.fromParts("sms",phoneNumber,null)
        );
        startActivity(intent);
    }

    private void sendSMS(String phoneNumber, String smsBody) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber,null,smsBody,null,null);
        }catch (Exception e){
            if (e.toString().contains(Manifest.permission.READ_PHONE_STATE) && ContextCompat
                    .checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)!=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                        .READ_PHONE_STATE}, READ_PHONE_STATE_REQUEST_CODE);
            }
        }
    }
}
